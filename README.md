## Sortowanie obiektów przez scalanie - wersja iteracyjna
Sortowanie przez scalanie (ang. merge sort) to algorytm sortowania danych stosujący metodę dziel i zwyciężaj. Polega na podzieleniu zbioru danych na najmniejsze możliwe (jednoelementowe) podzbiory, a następnie ich kolejne łączenie w coraz większe kolekcje. Algorytm nominalnie został zaprojektowany w wersji rekurencyjnej, jednak jest możliwa jego implementacja w wersji iteracyjnej (taki też był cel tego ćwiczenia).

### Złożoność obliczeniowa
Algorytm ma identyczną złożoność obliczeniową dla danych posortowanych, posortowanych malejąco i o losowej kolejności. Jest to spowodowane tym, że niezależnie od danych wejściowych są one rozdzielane na ww. podzbiory i następnie ponownie składane w jeden duży.  
![](https://wikimedia.org/api/rest_v1/media/math/render/svg/c1af7d3eabedd251de3629edd71e750fc1a8c4e7)  
Po rozwinięciu nawiasów otrzymuje się, że: ![.](https://wikimedia.org/api/rest_v1/media/math/render/svg/d7cec3e7349873dbe5ebbbf1ec4a60eecad34130)  
Asymptotyczna złożoność sortowania przez scalanie wynosi więc `O(n log n)`.

### Pomiary czasu  
![](plot.png)  

| liczba elementów | posortowane | malejące | losowe |
| ----------| ----------------------| ----------------------| ----------------------|
|1000	    | 0.00040746927261353	| 0.00038853883743286	| 0.00039018392562866   |
|2000	    | 0.00079696178436279	| 0.00077968835830688	| 0.00078079700469971   |
|4000	    | 0.0015725016593933	| 0.001563549041748	    | 0.0015606284141541    |
|8000	    | 0.0031288981437683	| 0.0031221151351929	| 0.0031195759773254    |
|16000	    | 0.0060445070266724	| 0.0060451030731201	| 0.0060328483581543    |
|32000	    | 0.012006747722626	    | 0.012045860290527	    | 0.011936569213867     |
|64000	    | 0.024036538600922	    | 0.024060726165771	    | 0.02401670217514      |
|96000      | 0.036251139640808     | 0.036192154884338     | 0.036146593093872     |
|128000	    | 0.048108077049255	    | 0.047992050647736	    | 0.048664915561676     |
|164000     | 0.061748075485229     | 0.061528134346008     | 0.061573195457458     |
|256000	    | 0.096215724945068	    | 0.096316051483154	    | 0.097231435775757     |
|384000	    | 0.14394207000732	    | 0.14383494853973	    | 0.14584908485413      |
|512000	    | 0.19177930355072	    | 0.19161591529846	    | 0.19421944618225      |
|768000	    | 0.28700804710388	    | 0.28810040156047	    | 0.29062644640605      |
|1024000	| 0.38347864151001	    | 0.38305068016052	    | 0.38689525922139      |

<?php
/**
 * Created by PhpStorm.
 * User: michal.markiewicz
 * Date: 16.10.2016
 * Time: 15:48
 */

namespace App;


class Sorter
{
    /**
     * @param Sortable[] $data
     */
    public static function sort(array &$data)
    {
        $sorted = [];
        $count = count($data);
        for ($part = 1; $part < $count; $part *= 2)
            for ($start = 0; $start < $count; $start += 2 * $part)
                self::merge($data, $sorted, $start, $start + $part, $start + 2 * $part);
    }
    
    /**
     * @param $data   Sortable[]
     * @param $sorted Sortable[]
     * @param $lo     integer
     * @param $mid    integer
     * @param $hi     integer
     */
    private static function merge(&$data, &$sorted, $lo, $mid, $hi)
    {
        $count = count($data);
        if ($mid >= $count) return;
        if ($hi > $count) $hi = $count;
        $i = $lo;
        $j = $mid;
        for ($k = $lo; $k < $hi; $k++) {
            if ($i === $mid)
                $sorted[$k] = $data[$j++];
            elseif ($j === $hi)
                $sorted[$k] = $data[$i++];
            elseif ($data[$j]->compareTo($data[$i]) > 0)
                $sorted[$k] = $data[$j++];
            else
                $sorted[$k] = $data[$i++];
        }
        $data = array_slice($sorted, $lo, $hi - $lo);
    }
}
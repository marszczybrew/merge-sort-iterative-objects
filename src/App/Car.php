<?php
/**
 * Created by PhpStorm.
 * User: michal.markiewicz
 * Date: 16.10.2016
 * Time: 15:49
 */

namespace App;


class Car implements Sortable
{
    /**
     * @var integer
     */
    private $power;
    
    public function __construct($power)
    {
        $this->power = $power;
    }
    
    /**
     * {@inheritDoc}
     */
    public function compareTo($o)
    {
        if ($o instanceof Car::class) {
            /** @var Car $o */
            if ($this->power === $o->power)
                return 0;
            else
                return $this->power < $o->power;
        }
        return 1;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: michal.markiewicz
 * Date: 16.10.2016
 * Time: 15:49
 */

namespace App;


class Wallet implements Sortable
{
    private $cash;
    
    public function __construct($cash)
    {
        $this->cash = $cash;
    }
    
    /**
     * {@inheritDoc}
     */
    public function compareTo($o)
    {
        if ($o instanceof $this) {
            /** @var Wallet $o */
            if ($o->cash === $this->cash)
                return 0;
            else
                return $this->cash < $o->cash;
        }
        return 1;
    }
    
    public function getCash() {
        return $this->cash;
    }
}
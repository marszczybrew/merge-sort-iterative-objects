<?php
/**
 * Created by PhpStorm.
 * User: michal.markiewicz
 * Date: 16.10.2016
 * Time: 15:49
 */

namespace App;


interface Sortable
{
    /**
     * @param $o
     *
     * @return integer <ul>
     *                 <li>1 if bigger,</li>
     *                 <li>0 if equal,</li>
     *                 <li>-1 if smaller</li>
     *                 </ul>
     */
    public function compareTo($o);
}
<?php
// Routes

use Slim\Http\Request;
use Slim\Http\Response;
use Tests\Unit\SorterTest;

$app->get('/', function (Request $request, Response $response, array $args) {
    ini_set('memory_limit', '1024M');
    $this->logger->info("Slim-Skeleton '/' route");
    
    $ordered = 0;
    $descending = 0;
    $random = 0;
    $iterations = 10;
    $items = 1000000;
    
    $test = new SorterTest($items);
    for ($i = 0; $i < $iterations; $i++) {
        $ordered += $test->testOrderedData();
        $descending += $test->testDescendingData();
        $random += $test->testRandomData();
    }
    
    return $this->renderer->render($response, 'index.phtml', [
        'ordered'    => $ordered / $iterations,
        'descending' => $descending / $iterations,
        'random'     => $random / $iterations,
        'iterations' => $iterations,
        'count'      => $items,
    ]);
});

<?php
/**
 * Created by PhpStorm.
 * User: michal.markiewicz
 * Date: 16.10.2016
 * Time: 16:14
 */

namespace Tests\Unit;

use App\Sorter;
use App\Wallet;
use PHPUnit\Framework\TestCase;

class SorterTest extends TestCase
{
    /**
     * @var int how many objects should be tested at once
     */
    private $items;
    
    public function __construct($items = 1000, $name = "Sorter test", array $data = [], $dataName = "")
    {
        $this->items = $items;
        parent::__construct($name, $data, $dataName);
    }
    
    /**
     * @return float time taken to 'sort' 1000 ordered items
     */
    public function testOrderedData()
    {
        /** @var Wallet[] $data */
        $data = [];
        for ($i = 1; $i < $this->items; $i++) {
            $data[] = new Wallet($i);
        }
        
        $start_time = microtime(true);
        Sorter::sort($data);
        $end_time = microtime(true);
        
        $previous = $data[0];
        foreach ($data as $item) {
            assert($previous->getCash() <= $item->getCash());
        }
        
        return $end_time - $start_time;
    }
    
    /**
     * @return float sort 1000 items which are in descending order
     */
    public function testDescendingData()
    {
        /** @var Wallet[] $data */
        $data = [];
        for ($i = $this->items; $i > 0; $i--) {
            $data[] = new Wallet($i);
        }
        
        $start_time = microtime(true);
        Sorter::sort($data);
        $end_time = microtime(true);
        
        $previous = $data[0];
        foreach ($data as $item) {
            assert($previous->getCash() <= $item->getCash());
        }
        
        return $end_time - $start_time;
    }
    
    public function testRandomData()
    {
        /** @var Wallet[] $data */
        $data = [];
        for ($i = $this->items; $i > 0; $i--) {
            $data[] = new Wallet(random_int(0, 10000));
        }
        
        $start_time = microtime(true);
        Sorter::sort($data);
        $end_time = microtime(true);
        
        $previous = $data[0];
        foreach ($data as $item) {
            assert($previous->getCash() <= $item->getCash());
        }
        
        return $end_time - $start_time;
    }
}
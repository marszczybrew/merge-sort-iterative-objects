<?php

namespace Tests\Functional;

class HomepageTest extends BaseTestCase
{
    /**
     * Test that the index route returns a rendered response containing the text 'SlimFramework' but not a greeting
     */
    public function testGetHomepageWithoutName()
    {
        $response = $this->runApp('GET', '/');
        
//        static::assertEquals(200, $response->getStatusCode());
    }
    
    /**
     * Test that the index route won't accept a post request
     */
    public function testPostHomepageNotAllowed()
    {
        $response = $this->runApp('POST', '/', ['test']);
        
        static::assertEquals(405, $response->getStatusCode());
        static::assertContains('Method not allowed', (string)$response->getBody());
    }
}